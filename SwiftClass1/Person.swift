//
//  Person.swift
//  SwiftClass1
//
//  Created by Igor Maldonado Floôr on 03/02/18.
//  Copyright © 2018 IgorMaldonado. All rights reserved.
//

import UIKit

class Person: NSObject {
    
    var gender: Gender?
    
    enum Gender: String {
        case M = "Masc"
        case F = "Fem"
        case U = "Undef"
    }

}
