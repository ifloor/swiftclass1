//
//  ViewController.swift
//  SwiftClass1
//
//  Created by Igor Maldonado Floor on 02/02/18.
//  Copyright © 2018 IgorMaldonado. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var textArea: UITextView!
    
    var nome: String? = "Igor"
    
    enum student: String {
        case Marcelo = "Marcelo"
        case Paulo = "PC"
        case Charbel = "Charbs"
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //Type your code here
    func swiftLearning() {
        var student = Student()
        student.name = "Igor"
        student.age = 27
        student.lastname = "Maldonado"
        student.gender = Person.Gender.U
        
        textArea.text.append("A student: \(student.toString())")
        
    }
    
    func printNome(name: String) {
        print("meu nome é: \(name)")
    }
    
    @IBAction func buttonTapped(_ sender: Any) {
        self.swiftLearning()
    }
    
}

