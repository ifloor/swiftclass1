//
//  Student.swift
//  SwiftClass1
//
//  Created by Igor Maldonado Floôr on 03/02/18.
//  Copyright © 2018 IgorMaldonado. All rights reserved.
//

import UIKit

class Student: Person {
    var name: String
    var age: Int
    var lastname: String
    var grade: String!
    
    override init() {
        self.name = ""
        self.age = 0
        self.lastname = ""
    }
    
    func toString() -> String {
        var genderString = ""
        if let gender = self.gender {
            switch gender {
                case Gender.M:
                    genderString = "Masculino"
            case Gender.F:
                genderString = "Feminino"
            default:
                genderString = "Não definido"
            }
        } else {
            genderString = "Não informado"
        }
        
        
        
        
        return "name: \(self.name), age: \(self.age), lastname: \(self.lastname), grade: \(self.grade), gender: \(genderString)"
    }
}
